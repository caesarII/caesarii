# CAESARII

Realizzato con il coﬁnanziamento dell’Unione europea,  Programma Operativo Nazionale Governance e Capacità Istituzionale 2014-2020 – FSE/FESR con il contributo dell’Agenzia per la Coesione Territoriale nell’ambito del programma PON Governance e Capacità Istituzionale 2014/2020 – Asse 3 – Obiettivo Speciﬁco 3.1 – Azione 3.1.1. Si tratta di un’azione che mira a sostenere il trasferimento e la diffusione delle buone pratiche delle Pubbliche Amministrazioni italiane attraverso la piattaforma Open Community Pa2020. Il progetto della durata di 19 mesi (inizio 3 Maggio 2018 ﬁne 30 novembre 2019) è gestito dal Comune di Cava de’ Tirreni (Capoﬁla ed Ente riusante) in partenariato con il Centro Studi Plinivs Struttura operativa del Centro interdipartimentale di ricerca Laboratorio di Urbanistica e di Pianiﬁcazione del Territorio “Raffaele d’Ambrosio” – dell’Università degli Studi di Napoli Federico II, la Regione Campania (Enti Cedenti), la Regione Sicilia (Dipartimento della Protezione Civile e Comune di Nicolosi) e il Comune di Grumento Nova (Enti riusanti). Cup: J76E18000110006 – Codice Locale: ID-49 – SI_1_604.

# OBIETTIVO

Rendere disponibile alle Pubbliche Amministrazioni, sotto forma di riuso, uno strumento di valutazione e quantizzazione del danno sismico a scala comunale utile per la pianificazione e la gestione dell’emergenza.
Sono integrati strumenti per l'analisi costi-benefici e di analisi multicriterio a supporto delle decisioni per orientare scelte di politiche alternative indirizzate alla riduzione del rischio



