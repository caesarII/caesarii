<?php

include_once __DIR__.'/core.php';

$pageTitle = tr('Informazioni');

$paths = App::getPaths();

include_once App::filepath('include|custom|', 'top.php');

echo '
<div class="box">
    <div class="box-header">
        <img src="'.$paths['img'].'/logo.png" alt="'.tr('OSM Logo').'">
        <h3 class="box-title">'.tr('Plinivs').'</h3>
        <div class="pull-right">
            <i class="fa fa-info"></i> '.tr('Informazioni').'
        </div>
    </div>

    <div class="box-body">';

if (file_exists($docroot.'/assistenza.php')) {
    include $docroot.'/assistenza.php';
} else {
    echo '
        <div class="row">
            <div class="col-md-8">
                <p>'.tr('Procedura automatica per lo sviluppo delle valutazioni di impatto in tempo reale per effetto di sequenza pre-eruttive e della ricaduta da cenere nelle aree del Vesuvio e dei Campi Flegrei
ARES 2017
Aggiornamento analisi di Rischio e di Scenario al Vesucio e ai Campi Flegrei').'.</p>

               
            </div>

            <div class="col-md-4">
                <p><b>'.tr('Sito web').':</b> <a href="http://plinivs.it/home/" target="_blank">plinivs.it/home/</a></p>

                <p><b>'.tr('Versione').':</b> '.$version.' <small class="text-muted">('.(!empty($revision) ? 'R'.$revision : tr('In sviluppo')).')</small></p>

            </div>
        </div>

        <hr>

        <div class="row">

        </div>';
}

echo '

	</div>
</div>';

include_once App::filepath('include|custom|', 'bottom.php');
